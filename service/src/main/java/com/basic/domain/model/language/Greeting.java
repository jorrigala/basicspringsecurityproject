package com.basic.domain.model.language;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.basic.domain.Model;

@Entity
@Getter
@Setter
@Table(name="GREETINGS")
public class Greeting extends Model {
	
	@Column(name = "LANGUAGE")
	private String language;
	
	@Column(name = "GREETING")
	private String greeting;

}
