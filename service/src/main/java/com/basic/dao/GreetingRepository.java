package com.basic.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.basic.domain.model.language.Greeting;

@Repository
public interface GreetingRepository  extends JpaRepository<Greeting,String>{	
	
	public Greeting findByLanguageIgnoreCase(String language);

}
